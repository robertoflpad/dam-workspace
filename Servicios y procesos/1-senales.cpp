#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void handler_sigint (int sig){
    printf("Ctrl+C no valido \n");
}

void handler_sigusr1 (int sig) {
    printf("Has mandado la señal SIGUSR1");
}

int main(int argc, char *argv[]) {

  /*Signals:
   * kill e -SIGKILL <numproceso> --> mata el proceso
   * kill -s SIGUSR1 <PID>
   * Los que hemos dado
   * SIGINT
   * SIGKILL --> no se puede manipular con sigaction
   * SIGCHILD
   * SIGTERM
   *
   */
     struct sigaction sa;
     struct sigaction sa2;

     sa.sa_handler = &handler_sigint;
     sa2.sa_handler = &handler_sigusr1;

    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGUSR1, &sa2, NULL);

    for(int i=0; i<100; i++){
        printf("Buenas tardes\n");
        sleep(2);
    }


    return EXIT_SUCCESS;
}
