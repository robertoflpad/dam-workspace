#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct args {
    int a;
    int b;
};

void *thread_function (void *args) {

    /* Copia de la estructura */
    struct args pepe = *(struct args *) args;

    printf (" a =  %i\n b = %i\n", pepe.a, pepe.b);

    /* Puntero a la estructura */
//    struct args *pepe = (struct args *) args;

//    printf (" a =  %i\n b = %i\n", pepe->a, pepe->b);

    return NULL;
}

int main (int argc, char *argv[]) {

    pthread_t thread_id;
    struct args thread_args = {5, 10};

    pthread_create(&thread_id, NULL, &thread_function, (void *) &thread_args);

    pthread_join(thread_id, NULL);

    printf (" Hola");


    return EXIT_SUCCESS;
}
