#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct args {
    int a;
    int b;
};

void *thread_function (void *args) {

    /* Copia de la estructura */
//    struct args pepe = *(struct args *) args;

//    printf (" a =  %i\n b = %i\n", pepe.a, pepe.b);

/*--------------------------------------------------------*/
    /* Puntero a la estructura */
    struct args *pepe = (struct args *) args;

    printf (" a =  %i\n b = %i\n\n", pepe->a, pepe->b);

    pepe->a = 7;
    pepe->b = 14;
/*--------------------------------------------------------*/
    return NULL;
}

int main (int argc, char *argv[]) {

    pthread_t thread_id;
    struct args thread_args = {5, 10};

    printf("Los numeros son: %i y %i\n\n", thread_args.a, thread_args.b);

    pthread_create(&thread_id, NULL, &thread_function, (void *) &thread_args);

    pthread_join(thread_id, NULL);

    printf ("He cambiado el valor %i, %i\n\n", thread_args.a, thread_args.b);


    return EXIT_SUCCESS;
}
