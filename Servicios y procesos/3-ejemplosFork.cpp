#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main () {

	//pid_t es igual a un signed int (Entero con signo)

	signed int valorEntero;
	int var = 1;

	printf("++++++++++++++++++++++++++++++++\n|BUENAS TARDES DAMAS Y CABALLEROS|\n++++++++++++++++++++++++++++++++\n\n");

	printf("-VAR: HOLA, SOY ||var|| y tengo un valor que es: %d\n\n", var);

	valorEntero = fork();

		if(valorEntero == 0) {

			var = 33;

			sleep(1);
			printf("-HIJO: Hola buenas, soy el proceso HIJO, mi PID es: %d \n", getpid());
			printf("-HIJO: Perdóname var, no me gusta tu valor, en mi proceso tu vales esto: %d\n\n", var);
			sleep(2);
			printf("-VAR: Ahora valgo %d, ¡Valgo mucho que bien!\n\n", var);

		} else {
			sleep(5);
			var = 0;
			printf("-PADRE: Hola, yo soy el proceso PADRE, mi PID es: %d \n", getpid());
			printf("-PADRE: Hijo mio, me he quedado 5 segundos durmiendo, var no vale nada para mi, vale: %d\n\n", var);
			sleep(2);
			printf("-VAR: El proceso PADRE es un aguafiestas, ahora valgo %d, ¡Que asco!\n\n", var);
                }

	return EXIT_SUCCESS;
}
