#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <signal.h>

sem_t semaphore;
sig_atomic_t num = 0;

void *thread1_function (void *) {

    for(int i=0; i<10; i++){

        sem_wait(&semaphore);
        num++;
        sem_post(&semaphore);
    }

    return NULL;
}

void *thread2_function (void *) {

    for(int i=0; i<10; i++){
        sem_wait(&semaphore);
        num--;
        sem_post(&semaphore);
    }

    return NULL;
}
int main (int argc, char *argv[]) {
    /*
     * sem_t semaforo;
     *
     * ini sem_init(sem_t, int pshared, unsigned int value);
     * ini sem_wait(sem_t *sem);
     * int sem_post (sem_t *sem);
     *
     */

    sem_init(&semaphore, 0, 1);

    pthread_t thread1_id, thread2_id;

    pthread_create(&thread1_id, NULL, &thread1_function, NULL);
    pthread_create(&thread2_id, NULL, &thread2_function, NULL);
    
    printf("\t NUMBER: %i\n", num);

    return EXIT_SUCCESS;
}
