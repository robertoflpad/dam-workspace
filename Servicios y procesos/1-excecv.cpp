#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char * argv[]) {
    pid_t child_pid;

    printf("EXPLAIN EXECV & EXECVP");

    child_pid = fork();

    if(child_pid != 0){
        printf("Este es el proceso padre %i\n", getpid());
    } else {
        printf("Este es el proceso hijo %i, y este el del padre %i\n\n", getpid(), getppid());

    }
    return EXIT_SUCCESS;
}
