#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void child_process () {

    for(int i = 0; i<10; i++){
        printf("[PID: %i] --> Hola soy el proceso hijo \n\n", getpid());
        sleep(2);
    }
}

void parent_process() {
    for(int i = 0; i<1; i++){
        printf("[PID: %i] --> Soy la proceso padre \n\n", getpid());
    }

}

int main () {

    pid_t child_pid;
    int status;

    child_pid = fork();
  

    if(child_pid == 0){
        child_process();
    }else{
        parent_process();
        wait(&status);
    }

    return EXIT_SUCCESS;
}
