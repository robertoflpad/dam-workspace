#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

/*Signals:
   * kill e -SIGKILL <numproceso> --> mata el proceso
   *
   * Los que hemos dado
   * SIGINT
   * SIGKILL --> no se puede manipular con sigaction
   * SIGCHILD
   * SIGTERM
   *
   */

sig_atomic_t var = 0;

void handler_sigint (int sig) {
    var++;
    printf("No se puede utilizar CTRL+C.\nLa raviable vale: %i\n", var);
}

void handler_sigusr1 (int sig) {
    printf("Has mandado la señal SIGUSR1.\n");
}

int main (int argc, char *argv[]) {

    struct sigaction sa;
    struct sigaction sa2;


    sa.sa_handler = &handler_sigint;
    sa2.sa_handler = &handler_sigusr1;

    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGUSR1, &sa2, NULL);

    for(int i = 0; i<100; i++){
        printf("HOLA BUENOS DIAS\n\n");
        sleep(2);
    }


    return EXIT_SUCCESS;
}
