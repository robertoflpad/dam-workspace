#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *thread_function (void *num){

    int a = *((int *) num);

    printf("El numero es %i\n", a);
    
    //printf("El numero es %i\n", *(int *) num);

    return NULL;
}

int main(int argc, char *argv[]) {

    /* EQUIVALENCIA
     * fork --> pthread_create
     * wait --> pthread_join
     */

    pthread_t thread_id;
    int num = 5;

    pthread_create(&thread_id, NULL, &thread_function, (void *)&num);
    pthread_join(thread_id, NULL);
    printf("A diferencia de los procesos, este printf solo te lo ejecuta el hilo padre\n\n");


    return EXIT_SUCCESS;
}
