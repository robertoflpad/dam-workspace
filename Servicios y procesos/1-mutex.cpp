#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int number = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *thread1_function (void *args) {

    for(int i = 0; i<10; i++){
        pthread_mutex_lock(&mutex);
        printf("HILO 1: %i\n", ++number);
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}
void *thread2_function (void *args) {

    for(int i = 0; i<10; i++){
        pthread_mutex_lock(&mutex);
        printf("HILO 2: %i\n", --number);
        pthread_mutex_unlock(&mutex);
    }
    return NULL;
}

int main (int argc, char *argv[]) {

    pthread_t thread_id1, thread_id2;

    pthread_create(&thread_id1, NULL, &thread1_function, NULL);
    pthread_create(&thread_id2, NULL, &thread2_function, NULL);


    pthread_join(thread_id1, NULL);
    pthread_join(thread_id2, NULL);

    printf("NUMBER: %i", number);
    return EXIT_SUCCESS;
}
