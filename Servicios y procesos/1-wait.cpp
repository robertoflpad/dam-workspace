#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>


void parent_function(){
    printf("Soy el proceso padre\n");
}

void child_function(){
    for (int i = 0; i<500; i++){
        printf("ESPERAME\n\n");
        sleep(1);
    }
}


int main () {

    //pid_t es igual a un signed int (Entero con signo)
    int child_status;
    pid_t valorEntero;

    valorEntero = fork();

    if(valorEntero == 0) {

        child_function();
    } else {

        parent_function();

       // wait(&child_status);
    }

    return EXIT_SUCCESS;
}
