#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main () {

	//pid_t es igual a un signed int (Entero con signo)

	pid_t valorEntero;

	valorEntero = fork();

		if(valorEntero == 0) {

			printf("Hola buenas, soy el proceso HIJO, mi PID es: %d \n", getpid());

		} else {
			sleep(5);
			printf("Hola, yo soy el proceso PADRE, mi PID es: %d \n", getpid());

		}

	return EXIT_SUCCESS;
}
